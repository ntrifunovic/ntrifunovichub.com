#!/bin/sh

red='\x1B[0;31m'
brown='\x1B[0;33m'  
NC='\x1B[0m' # No Color

echo ${red}On GitHub [Pages]${NC}

# Submodule Pages
cd pages

echo ${brown}Removing deleted files${NC}
git add -u

echo ${brown}Adding new files${NC}
git add .

git commit  -m "auto submit"
git push 

cd ..

echo ${red}"On BitBucket & Heroku"${NC}

echo ${brown}Removing deleted files${NC}
git add -u

echo ${brown}Adding new files${NC}
git add .

git commit  -m "auto submit"
git push

#cd generator
#
#echo ${red}On Heroku${NC}
#git push heroku

echo ${red}On GitHub [WebSite]${NC}

cp -r static ../

cd ..

echo ${brown}Removing deleted files${NC}
git add -u

echo ${brown}Adding new files${NC}
git add .

git commit  -m "auto submit"
git push
