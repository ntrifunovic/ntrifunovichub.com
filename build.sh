#!/bin/sh

source venv/bin/activate 

echo Cleaning up

rm -r build/*

echo Start building
python site.py build
echo Done building

deactivate

cp -r  build/*  ../
cp -r  static ../

