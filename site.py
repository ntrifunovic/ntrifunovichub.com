# file:   site.py
# author: ntrifunovic

import os
import sys

from flask import Flask, request, url_for, render_template
from flask_flatpages import FlatPages, pygments_style_defs
from flask_frozen import Freezer

import markdown
import mdx_mathjax

import yaml

if not 'HEROKU' in os.environ:
    DEBUG = True

FLATPAGES_EXTENSION = '.md'
FLATPAGES_MARKDOWN_EXTENSIONS = ['mathjax', 'codehilite', 'toc', 'abbr']

app = Flask(__name__)
app.config.from_object(__name__)

structure = yaml.load_all(file('pages/structure.yaml', 'r'))

topics = next(structure)
topics = dict(zip(topics, range(len(topics))))

titles = next(structure)

for topic, title_names in titles.iteritems():
    titles[topic] = dict(zip(title_names, range(len(title_names))))

pages = FlatPages(app)

@app.template_filter('sort_topics')
def sort_topics(value):
    return sorted(value, key = lambda topic: topics[topic[0]]) 

@app.template_filter('sort_titles')
def sort_titles(value):
    return sorted(value, key = lambda page: titles[page['topic']][page['title']]) 

freezer = Freezer(app, with_static_files = False)

@app.route('/pygments.css')
def pygments_css():
    return pygments_style_defs('native'), 200, {'Content-Type': 'text/css'}

@app.route('/<path:path>/')
def page(path):
    return render_template('doc.html', output = pages.get_or_404(path), pages = pages)

@app.route('/')
def index():
    return page('home')

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1] == "build":
        freezer.freeze()
    else:
        port = int(os.environ.get('PORT', 5000))
        app.run(host='0.0.0.0', port=port)
