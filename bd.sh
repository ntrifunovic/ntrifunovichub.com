#!/bin/sh

BOLD="\033[1m" #BOLD
R='\x1B[0;31m'
NC='\x1B[0m' # No Color

echo ${R}${BOLD}"BUILD 1/2"${NC}
./build.sh
echo ${R}${BOLD}"DEPLOY 2/2"${NC}
./deploy.sh
echo ${R}${BOLD}"ALL DONE :)"${NC}
